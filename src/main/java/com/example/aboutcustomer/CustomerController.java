package com.example.aboutcustomer;

import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {
    int id = 1;
    ArrayList<Customer> customers = new ArrayList<>();
     public CustomerController(){
         customers.add(new Customer(id++,"Vanneat","F",22,"PP"));
         customers.add(new Customer(id++,"Dara","M",22,"PP"));
     }

     @GetMapping("/api/v1/customers")
    public ArrayList<Customer> getAllCustomers(){
         return customers;
    }

//    insert into arraylist
    @PostMapping("/api/v1/cutomers")
    public Customer insertCustomer(@RequestBody CustomerReq  customerReq){
         Customer  customer = new Customer();
         customer.setId(id);
         customer.setName(customerReq.getName());
         customer.setAge(customerReq.getAge());
         customer.setGender(customerReq.getGender());
         customer.setAddress(customerReq.getAddress());
         id++;
         customers.add(customer);
         return customer;
    }

    //get customer by id
    @GetMapping("/api/v1/customers/{id}")
    public  Customer getCustomerById(@PathVariable("id") Integer customerId){
         for (Customer customer: customers){
             if (customer.getId()==customerId){
                 return  customer;
             }
         }
         return  null;
    }


    //Find customer by name
    @GetMapping("/api/v1/customers/search")
    public  Customer findCustomerByName(@RequestParam String name){
         for (Customer customer: customers){
             if ((customer.getName().equals(name)))
                 return  customer;
         }
         return null;
    }
    @PutMapping("/api/v1/customers/{id}")
    public ResponseEntity<Customer> updateCustomerById(
            @RequestBody CustomerReq customerReq,
            @PathVariable ("id") Integer cusId
    ) {
        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getId() == cusId) {
                Customer customer = new Customer();
                customer.setId(cusId);
                customer.setName(customerReq.getName());
                customer.setAge(customerReq.getAge());
                customer.setGender(customerReq.getGender());
                customer.setAddress(customerReq.getAddress());

                customers.set(cusId-1,customer);
                return ResponseEntity.ok(customer);
            }
        }
        return null;
    }

    @DeleteMapping("/api/v1/customers/{id}")
    public ResponseEntity<Customer> deleteCustomerByid(  @PathVariable ("id") Integer deleteid ) {
        for (Customer customer : customers) {
            if (customer.getId() == deleteid) {
                customers.remove(customer);
            }
        }
        return null;
    }
}
