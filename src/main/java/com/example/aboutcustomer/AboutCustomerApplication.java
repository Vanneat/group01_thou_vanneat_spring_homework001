package com.example.aboutcustomer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AboutCustomerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AboutCustomerApplication.class, args);
    }

}
